package com.example.mustafa.a_market;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Slider_Activity extends AppCompatActivity {


    ViewPager viewPager1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slider);

        viewPager1 = (ViewPager) findViewById(R.id.viewPager1);


        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(this);
        viewPager1.setAdapter(viewPagerAdapter);

    }
}
